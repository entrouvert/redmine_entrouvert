module AttachmentsControllerPatch
  def self.included(base)
    base.class_eval do
      def show
        respond_to do |format|
          format.html {
            if @attachment.is_diff?
              @diff = File.new(@attachment.diskfile, "rb").read
              @diff_type = params[:type] || User.current.pref[:diff_type] || 'inline'
              @diff_type = 'inline' unless %w(inline sbs).include?(@diff_type)
              @diff_format_patch = nil;
              @diffstat_format_patch = nil;
              if (@diff.start_with?("From") and @diff.match(/(Subject.*)^---$/m))
                @diff_format_patch = @diff.match(/(Subject.*)^---$/m)[0].sub(/^---$/m, '').force_encoding('UTF-8');
                if (@diff.match(/^---$(.*?)^diff/m))
                  @diffstat_format_patch = @diff.match(/^---$(.*?)^diff/m)[1].sub(/^---$/m, '').force_encoding('UTF-8').rstrip;
                end
              end

              # Save diff type as user preference
              if User.current.logged? && @diff_type != User.current.pref[:diff_type]
                User.current.pref[:diff_type] = @diff_type
                User.current.preference.save
              end
              render :action => 'diff'
            elsif @attachment.is_text? && @attachment.filesize <= Setting.file_max_size_displayed.to_i.kilobyte
              @content = File.new(@attachment.diskfile, "rb").read
              render :action => 'file'
            else
              download
            end
          }
          format.api
        end
      end

      def disposition(attachment)
        if attachment.is_pdf? || attachment.is_image?
          'inline'
        else
          'attachment'
        end
      end

    end
  end
end

# Add module to Attachments Controller
AttachmentsController.send(:include, AttachmentsControllerPatch)
