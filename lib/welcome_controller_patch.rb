module WelcomeControllerPatch
   def self.included(base)
     base.class_eval do
       def index
         @news = News.latest User.current
         @projects = Project.all_public.active.order("LOWER(name)").find_all { |project| not project.repository.nil? }
       end
     end
   end
end

# Add module to Welcome Controller
WelcomeController.send(:include, WelcomeControllerPatch)
