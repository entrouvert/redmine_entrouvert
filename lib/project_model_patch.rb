module ProjectModelPatch
  def self.included(base)
    base.class_eval do
      # Returns a short description of the projects (first lines)
      def short_description(length = 255)
        description.lines.first.strip if description and description.lines.first
      end
    end
  end
end

Project.send(:include, ProjectModelPatch)
