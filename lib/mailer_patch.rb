module MailerPatch
  def self.included(base)
    base.class_eval do
      def self.token_for(object, rand=true)
        timestamp = object.send(object.respond_to?(:created_on) ? :created_on : :updated_on)
        hash = [
          "redmine",
          "#{object.class.name.demodulize.underscore}-#{object.id}",
          timestamp.strftime("%Y%m%d%H%M%S")
        ]
        # disabled, see https://dev.entrouvert.org/issues/6582
        #if rand
        #  hash << Redmine::Utils.random_hex(8)
        #end
        host = Setting.mail_from.to_s.gsub(%r{^.*@}, '')
        host = "#{::Socket.gethostname}.redmine" if host.empty?
        "#{hash.join('.')}@#{host}"
      end
    end
  end
end

Mailer.send(:include, MailerPatch)
