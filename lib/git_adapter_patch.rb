require 'redmine/scm/adapters/git_adapter.rb'

# skip wip/ branches

module Redmine
  module Scm
    module Adapters
      class GitAdapter

        alias_method :branches_original, :branches

        def branches(*args)
          all_branches = branches_original(*args)
          return [] if !all_branches
          all_branches.find_all { |b| !b.start_with?("wip/") }
        end
      end
    end
  end
end
