require 'redmine'

require_dependency 'welcome_controller_patch'
require_dependency 'project_model_patch'
require_dependency 'mailer_patch'
require_dependency 'attachments_controller_patch'
require_dependency 'git_adapter_patch'
require_dependency 'account_controller_patch'

Redmine::Plugin.register :redmine_entrouvert do
  name 'Redmine Entr\'ouvert plugin'
  author 'Entr\'ouvert'
  version '0.2.0'
end
